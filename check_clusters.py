# !/usr/bin/ksh
# Written by Lorenzo Ramirez
# Function: to monitor the state of Arden's Pacemaker/Corosync clusters using crm_mon command

# imports necessary packages
import subprocess
import sys
import shutil
import os

# Nagios States
STATE_OK = 0
STATE_WARNING = 1
STATE_CRITICAL = 2
STATE_UNKNOWN = 3

# crm_mon path
CRM_MON = "/usr/sbin/crm_mon"

# sudo path
SUDO = "/bin/sudo"

# Error Messages
catch_all = "UNKNOWN: Please check configuration"


def main():
    crm_return_code = None
    crm_stdout = None

    try:
        # Checks for crm_mon's existence
        crm_exists = shutil.which(CRM_MON) is not None

        # Runs crm_mon command
        crm = subprocess.run([SUDO, CRM_MON, "-s"], stdout=subprocess.PIPE)

        # Captures crm_mon's return code and stdout
        crm_return_code = crm.returncode
        crm_stdout = crm.stdout

    except FileNotFoundError as file_err:
        crm_return_code = 1
        crm_stdout = "UNKNOWN: File not found with error {err}".format(err=str(file_err))
    except PermissionError as permission_err:
        crm_return_code = 1
        crm_stdout = "UNKNOWN: Permission not granted with error {err}".format(err=str(permission_err))
    except OSError as os_err:
        crm_return_code = 1
        crm_stdout = "UNKNOWN: System related error with error {err}".format(err=str(os_err))
    except:
        crm_return_code = 1
        crm_stdout = catch_all

    # Prints crm's standard format
    print(crm_stdout)

    # Exits program with crm's return code
    sys.exit(crm_return_code)


if __name__ == "__main__":
    main()
